module.exports = {
    title: 'Openhe\'s blog',
    description: '我的个人博客网站',
    theme: 'reco',
    head: [ 
      ['link', { rel: 'icon', href: '/logo.jpg' }], 
    ],
    base: '/blog/', 
    markdown: {
      lineNumbers: false 
    },
    themeConfig: {
      nav:[ 
        {
            text:'测试',
            items:[
                {text:'笔记一',link:'/test/note1.md'},
                {text:'笔记二',link:'/test/note2.md'}
            ]
        },
        {text: '我的gitee', link: 'https://gitee.com/openhe'}      
      ],
      sidebar: 'auto', 
      sidebarDepth: 2, 
    }
  };